#pragma once
using namespace std;

class TicTacToe
{
private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn, m_winner;

public:

	void Move(int position) { m_board[(position - 1)] = m_playerTurn; }

	char GetPlayerTurn() { return m_playerTurn; }

	//constructor
	TicTacToe()
	{
		for (int i = 0; i < 9; i++)
			m_board[i] = ' ';

		m_playerTurn = '@'; 
		m_winner = ' ';
		m_numTurns = 0;
	}

	// methods
	void DisplayBoard() const
	{
		cout << "+-------+-------+-------+\n"
			<< "|1      |2      |3      |\n"
			<< "|   " << m_board[0] << "   |   " << m_board[1] << "   |   " << m_board[2] << "   |\n"
			<< "|       |       |       |\n"
			<< "+-------+-------+-------+\n"
			<< "|4      |5      |6      |\n"
			<< "|   " << m_board[3] << "   |   " << m_board[4] << "   |   " << m_board[5] << "   |\n"
			<< "|       |       |       |\n"
			<< "+-------+-------+-------+\n"
			<< "|7      |8      |9      |\n"
			<< "|   " << m_board[6] << "   |   " << m_board[7] << "   |   " << m_board[8] << "   |\n"
			<< "|       |       |       |\n"
			<< "+-------+-------+-------+\n";
	}

	void DisplayResult() const
	{
		if (m_winner != ' ')
			cout << "The winner is " << m_winner << "!!!\n";
		else
			cout << "It's a tie!!!\n";
	}

	bool IsOver()
	{
		if (m_board[0] == m_playerTurn && m_board[1] == m_playerTurn && m_board[2] == m_playerTurn || // horizontal rows
			m_board[3] == m_playerTurn && m_board[4] == m_playerTurn && m_board[5] == m_playerTurn ||
			m_board[6] == m_playerTurn && m_board[7] == m_playerTurn && m_board[8] == m_playerTurn ||

			m_board[0] == m_playerTurn && m_board[3] == m_playerTurn && m_board[6] == m_playerTurn || // vertical rows
			m_board[1] == m_playerTurn && m_board[4] == m_playerTurn && m_board[7] == m_playerTurn ||
			m_board[2] == m_playerTurn && m_board[5] == m_playerTurn && m_board[8] == m_playerTurn ||

			m_board[0] == m_playerTurn && m_board[4] == m_playerTurn && m_board[8] == m_playerTurn || // diagonal 
			m_board[2] == m_playerTurn && m_board[5] == m_playerTurn && m_board[6] == m_playerTurn)
		{
			m_winner = m_playerTurn;
			return true;
		}
		else
		{
			if (m_numTurns < 9)
			{
				if (m_playerTurn == 'X')
					m_playerTurn = 'O';
				else
					m_playerTurn = 'X';
				return false;
			}
			else
				return true;
		}
	}

	bool IsValidMove(int position)
	{
		if (position >= 1 && position <= 9)
		{
			if (m_board[(position - 1)] == ' ')
			{
				m_numTurns++;
				return true;
			}
			else
				return false;
		}
		else
		{
			cout << "Invalid input, your number was too big or too small!!!\n";
			return false;
		}
	}
};